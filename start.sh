#!/bin/sh
#cp ./app/etc/sysctl.conf /etc/sysctl.conf
#sysctl -p ./app/etc/sysctl.conf
case $1 in
start)
        docker-compose down
        docker-compose start
        ;;
force-start)
        docker-compose down
        docker-compose up -d --no-deps --build
        ;;
down)
        docker-compose down
        ;;
log)
        docker-compose logs -f
        ;;
*)
        docker-compose down
        docker-compose up -d --no-deps --build
esac

